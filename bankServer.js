global.fetch = require("node-fetch");
global.WebSocket = require("ws");

request = require('request')
const fetch = require('node-fetch');
const { TextEncoder, TextDecoder } = require('util');
const { Api, JsonRpc, RpcError } = require('eosjs');
const { JsSignatureProvider } = require('eosjs/dist/eosjs-jssig'); 

const { createDfuseClient, InboundMessageType } = require("@dfuse/client")

const client = createDfuseClient({ apiKey: "server_36ca10151a5d744ee6a28e685ba68b51", network: "jungle" })

const defaultPrivateKey = "5JfbS6SH5bk8F4GUVANGX68e3GQ8BbRtDQZNpNebJo8EkSbfK32"; // bob
const signatureProvider = new JsSignatureProvider([defaultPrivateKey]);

const rpc = new JsonRpc('https://jungle2.cryptolions.io:443', { fetch });

const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });


// FIREBASE
var admin = require("firebase-admin");
var serviceAccount = require("./hlflightstatusportal-firebase-adminsdk-5dge4-7102301fef.json");
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://hlflightstatusportal.firebaseio.com"
});
var realtime_database = admin.database();
// FIREBASE


// DETECT NEW FLIGHT
client.streamActionTraces({ accounts: "assurancehlg", action_names: "newflight1" }, async (message) => {
  if (message.type === InboundMessageType.ACTION_TRACE) {
    const { flight, sta } = message.data.trace.act.data;
    console.log(`\nNew flight [${flight})]`);
    
    var flight_number = flight;
    var flight_timestamp = sta;
    console.log('\x1b[32m%s\x1b[0m', `New flight ${flight_number} found ${timePrinter(flight_timestamp*1000)}\n`);

    var flightIdentity = `${flight}:${sta}`;
    var newFlight = {
      sta: sta,
      ata: 0,
      atd: 0,
      status: 'WAITING'
    };
    var updates = {};
    updates['/flights/' + flightIdentity] = newFlight;

    await realtime_database.ref().update(updates);
  }
}).catch((error) => {
  console.log("An error occurred on {assurancehlg::newflight1}.", error)
})

// DETECT COMPENSATION REFUND
client.streamActionTraces({ accounts: "assurancehlg", action_names: "refund2" }, (message) => {
  if (message.type === InboundMessageType.ACTION_TRACE) {
    const { policy_no, policy_type, flight, sta, acc_number, statuss, compensation } = message.data.trace.act.data;
    console.log('\x1b[33m%s\x1b[0m', `${statuss}, refunding of RM${compensation} to ${acc_number} for flight ${flight} is proccessing...`);
    console.log(`\tDetails`);
    console.log(`\tPolicy  : ${policy_no}`);
    console.log(`\tType    : ${policy_type == 1? 'PREMIUM':'LITE'}`);
    console.log(`\tAccount : ${acc_number}`);
    console.log(`\tAmount  : RM${compensation}\n`);
  }
}).catch((error) => {
  console.log("An error occurred on {assurancehlg::refund1}.", error)
})

// DETECT depart
client.streamActionTraces({ accounts: "assurancehlg", action_names: "fdepart" }, async (message) => {
  if (message.type === InboundMessageType.ACTION_TRACE) {
    const { flight, atd, sta } = message.data.trace.act.data;
    console.log('\x1b[32m%s\x1b[0m', `\n${timePrinter(atd*1000)} ${flight} departed\n`);
    await realtime_database.ref(`flights/${flight}:${sta}`).transaction(function(post) {
      if (post) {
        post.atd = atd;
        post.status = 'DEPARTED';
      }
      return post;
    });
  }
}).catch((error) => {
  console.log("An error occurred on {assurancehlg::refund1}.", error)
});

// DETECT arrive
client.streamActionTraces({ accounts: "assurancehlg", action_names: "farrive" }, async (message) => {
  if (message.type === InboundMessageType.ACTION_TRACE) {
    const { flight, ata, sta } = message.data.trace.act.data;
    console.log('\x1b[32m%s\x1b[0m', `\n${timePrinter(ata*1000)} ${flight} arrival\n`);
    await realtime_database.ref(`flights/${flight}:${sta}`).transaction(function(post) {
      if (post) {
        post.ata = ata;
        post.status = 'LANDED';
      }
      return post;
    });
  }
}).catch((error) => {
  console.log("An error occurred on {assurancehlg::refund1}.", error)
});

// DETECT cancel
client.streamActionTraces({ accounts: "assurancehlg", action_names: "flightcl2" }, async (message) => {
  if (message.type === InboundMessageType.ACTION_TRACE) {
    const { flight, sta, cancelt } = message.data.trace.act.data;
    console.log('\x1b[33m%s\x1b[0m', `\nFlight ${flight} cancelled\n`);
    await realtime_database.ref(`flights/${flight}:${sta}`).transaction(function(post) {
      if (post) {
        post.status = 'CANCELLED';
      }
      return post;
    });
  }
}).catch((error) => {
  console.log("An error occurred on {assurancehlg::refund1}.", error)
});

function timePrinter(ms){
  var time = new Date(ms)
  //var ms = time.getMilliseconds()
  //time = Math.floor(time / 1000)
  //var s = time.getSeconds()
  //time = Math.floor(time / 60)
  //var m = time.getMinutes()
  //time = Math.floor(time / 60)
  //var h = time.getHours()//GMT+8
  //return '['+('00' + h).substr(-2)+':'+('00' + m).substr(-2)+':'+('00' + s).substr(-2)+':'+('000' + ms).substr(-3)+'] '
  return time.toUTCString();
}

console.log();
console.log('This program simulate the workflow of Bank Server');
console.log();
console.log('basically the bank server has 3 jobs, which are : ');
console.log('\t- send user policy to chain (when user BUY/GET any flight insurance)');
//console.log('\t\t-> ');
console.log();
console.log('\t- receive flight status change');
// console.log('\t- receive flight status change, save it in database for render purpose');
// console.log('\t\t-> in prototype, flight status will be saved in firebase');
console.log();
console.log('\t- receive any refund of compensation signal, and process the payment');
console.log('\t\t-> in prototype, message will be printed out once receive signal');
console.log('\n');

var apiCallResults = [];
// Bank Server
var express = require("express");
var app = express();
var bodyParser = require('body-parser')
app.use(bodyParser.json());
// app.get("/event", (req, res, next) => {
//   console.log(req.params);
//   res.json({
//     success: true
//   })
// });

function add_single(req) {
  apiCallResults.push(api.transact({
    actions: [{
      account: 'assurancehlg',
      name: 'add1',
      authorization: [{
        actor: 'assurancehlg',
        permission: 'active',
      }],
      data: {
        flight: req.body.flight, 
        sta: req.body.sta, 
        acc_number: req.body.acc_number, 
        policy_type: req.body.policy_type, 
        compensation: req.body.compensation,
        connect_to: 0, 
        connect_from: 0
      },
    }]
  }, {
      blocksBehind: 3,
      expireSeconds: 30,
  }).then(function(){
    console.log('Success push to EOSIO blockchain');
  }).catch(function(e){
      console.log('\x1b[33m%s\x1b[0m', '\tS:Got error, resend '+e);
      add_single(req);
  }));
}

app.post("/add_single", async (req, res, next) => {
  console.log('Received transaction, push flight insurance policy to EOSIO');
  add_single(req);
  res.json({
    success: true
  })
});

function add_connect(req, num) {
  apiCallResults.push(api.transact({
    actions: [{
      account: 'assurancehlg',
      name: 'addconnect',
      authorization: [{
        actor: 'assurancehlg',
        permission: 'active',
      }],
      data: {
        flight1: req.body.first.flight, 
        sta1: req.body.first.sta, 
        flight2: req.body.second.flight, 
        sta2: req.body.second.sta, 
        acc_number: req.body.acc_number,  
        compensation: req.body.compensation,
      },
    }]
  }, {
    blocksBehind: 3,
    expireSeconds: 30,
  }).then(function(){
    console.log('Success push to EOSIO blockchain');
  }).catch(function(e){
    if(num < 3) {
      console.log(req.body)
      add_connect(req. num+1);
      console.log('\x1b[33m%s\x1b[0m', '\tC:Got error, resend');
    }else{
      console.log('\x1b[33m%s\x1b[0m', `${e}`);
    }
  }));
}

app.post("/add_connect", async (req, res, next) => {
  console.log('Received transaction, push flight insurance policies to EOSIO');
  add_connect(req, 0);
  res.json({
    success: true
  })
});
app.listen(4002, () => {
 console.log("Bank Server running on port 4002");
});