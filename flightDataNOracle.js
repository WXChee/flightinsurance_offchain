global.fetch = require("node-fetch");
global.WebSocket = require("ws");

request = require('request')
const fetch = require('node-fetch');
const { TextEncoder, TextDecoder } = require('util');
const { Api, JsonRpc, RpcError } = require('eosjs');
const { JsSignatureProvider } = require('eosjs/dist/eosjs-jssig'); 

const { createDfuseClient, InboundMessageType } = require("@dfuse/client")

const client = createDfuseClient({ apiKey: "server_36ca10151a5d744ee6a28e685ba68b51", network: "jungle" })

const oraclePrivateKey = "5Km2kuu7vtFDPpxywn4u3NKydrLLG5isZ6rYkcw3xsH9hNYeA6i";
const hlgPrivateKey = "5JfbS6SH5bk8F4GUVANGX68e3GQ8BbRtDQZNpNebJo8EkSbfK32";
const signatureProvider = new JsSignatureProvider([oraclePrivateKey, hlgPrivateKey]);

const rpc = new JsonRpc('https://jungle2.cryptolions.io:443', { fetch });

const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });

async function flightdepart(flight, sta, atd){
  const result = await api.transact({
    actions: [{
      account: 'assurancehlg',
      name: 'fdepart',
      authorization: [{
        actor: 'flightoracle',
        permission: 'active',
      }],
      data: {
        flight: flight,
        sta: sta,
        atd: atd
      },
    }]
  }, {
    blocksBehind: 3,
    expireSeconds: 30,
  });
  console.dir(`Flight ${flight} is departed on ${timePrinter(atd)}, set on smart contract`);
}

async function flightarrive(flight, sta, ata){
  const result = await api.transact({
    actions: [{
      account: 'assurancehlg',
      name: 'farrive',
      authorization: [{
        actor: 'flightoracle',
        permission: 'active',
      }],
      data: {
        flight: flight,
        sta: sta,
        ata: ata
      },
    }]
  }, {
    blocksBehind: 3,
    expireSeconds: 30,
  });
  console.dir(`Flight ${flight} is arrived on ${timePrinter(ata)}, set on smart contract`);
}

async function flightcancel(flight, sta){
  const result = await api.transact({
    actions: [{
      account: 'assurancehlg',
      name: 'flightcl',
      authorization: [{
        actor: 'flightoracle',
        permission: 'active',
      }],
      data: {
        flight: flight,
        sta: sta
      },
    }]
  }, {
    blocksBehind: 3,
    expireSeconds: 30,
  });
  console.dir(`Flight ${flight} is cancelled, set on smart contract`);
}

client.streamActionTraces({ accounts: "assurancehlg", action_names: "newflight1" }, async (message) => {
  if (message.type === InboundMessageType.ACTION_TRACE) {
    const { flight, sta } = message.data.trace.act.data;
    console.log(`\nNew flight [${flight})]`);
    
    var flight_number = flight;
    var flight_timestamp = sta;
    console.log('\x1b[32m%s\x1b[0m', `New flight ${flight_number} found ${timePrinter(flight_timestamp*1000)}\n`);
  }
}).catch((error) => {
  console.log("An error occurred on {assurancehlg::newflight1}.", error)
})

// -------------------- FAKE API & TEST CASE --------------------
/* TEST CASE
  AK860 (on time)
  AK861 (delay 1h)
  EK347 (on time) -> (flight) EK029 (on time)
  EK340 (delay 2h) -> (flight) EK029 (on time)
  EK340 (delay 2h) -> (misconnection (time contrainst)) EK002 ()
  EK316 (delay 3h) -> (misconnection (departed)) EK029 (on time)
  EK316 (delay 3h) -> (flight) EK025 (delay 2h)

  AK860 1568278800
  AK861 1568284200
  EK347 1568316000
  EK340 1568316000
  EK316 1568319600
  EK029 1568343600
  EK002 1568340000
  EK025 1568343600
*/
var eventLists = [
  {
    flight:{
      flight_number:'AK860',
      flight_timestamp:1568278800
    },
    event:'departed',
    time:1568275200
  },
  {
    flight:{
      flight_number:'AK860',
      flight_timestamp:1568278800
    },
    event:'arrived',
    time:1568278600
  },
  {
    flight:{
      flight_number:'AK861',
      flight_timestamp:1568284200
    },
    event:'departed',
    time:1568283600
  },
  {
    flight:{
      flight_number:'AK862',
      flight_timestamp:1568284000
    },
    event:'cancel',
    time:1568283720
  },
  {
    flight:{
      flight_number:'EK347',
      flight_timestamp:1568316000
    },
    event:'departed',
    time:1568287200
  },
  {
    flight:{
      flight_number:'EK340',
      flight_timestamp:1568316000
    },
    event:'departed',
    time:1568287200
  },
  {
    flight:{
      flight_number:'AK861',
      flight_timestamp:1568284200
    },
    event:'arrived',
    time:1568289000
  },
  {
    flight:{
      flight_number:'EK316',
      flight_timestamp:1568319600
    },
    event:'departed',
    time:1568298600
  },
  {
    flight:{
      flight_number:'EK347',
      flight_timestamp:1568316000
    },
    event:'arrived',
    time:1568316000
  },
  {
    flight:{
      flight_number:'EK202',
      flight_timestamp:1568321000
    },
    event:'cancel',
    time:1568318000
  },
  {
    flight:{
      flight_number:'EK340',
      flight_timestamp:1568316000
    },
    event:'arrived',
    time:1568323800
  },
  {
    flight:{
      flight_number:'EK002',
      flight_timestamp:1568340000
    },
    event:'departed',
    time:1568325600
  },
  {
    flight:{
      flight_number:'EK201',
      flight_timestamp:1568343600
    },
    event:'cancel',
    time:1568327200
  },
  {
    flight:{
      flight_number:'EK029',
      flight_timestamp:1568343600
    },
    event:'departed',
    time:1568330400
  },
  {
    flight:{
      flight_number:'EK316',
      flight_timestamp:1568319600
    },
    event:'arrived',
    time:1568331000
  },
  {
    flight:{
      flight_number:'EK025',
      flight_timestamp:1568343600
    },
    event:'departed',
    time:1568335200
  },
  {
    flight:{
      flight_number:'EK002',
      flight_timestamp:1568340000
    },
    event:'arrived',
    time:1568340000
  },
  {
    flight:{
      flight_number:'EK029',
      flight_timestamp:1568343600
    },
    event:'arrived',
    time:1568343600
  },
  {
    flight:{
      flight_number:'EK025',
      flight_timestamp:1568343600
    },
    event:'arrived',
    time:1568351400
  }
];
/*
  AK860 1568278800
  AK861 1568284200
  EK347 1568316000
  EK340 1568316000
  EK316 1568319600
  EK029 1568343600
  EK002 1568340000
  EK025 1568343600
*/
insuranceList = [
    { flight:'AK860', sta: 1568278800, acc_number: 17151121991, policy_type: 0, compensation: 10 },
    { flight:'AK861', sta: 1568284200, acc_number: 17151121992, policy_type: 0, compensation: 10 },
    { flight:'AK861', sta: 1568284200, acc_number: 17151121996, policy_type: 1, compensation: 10 },
    { flight:'AK862', sta: 1568284000, acc_number: 17151121998, policy_type: 0, compensation: 10 },
    { flight:'EK347', sta: 1568316000, acc_number: 17151121993, policy_type: 1, compensation: 0 },
    { flight:'EK340', sta: 1568316000, acc_number: 17151121994, policy_type: 1, compensation: 0 },
    { flight:'EK340', sta: 1568316000, acc_number: 17151121995, policy_type: 1, compensation: 0 },
    { flight:'EK316', sta: 1568319600, acc_number: 17151121996, policy_type: 1, compensation: 0 },
    { flight:'EK316', sta: 1568319600, acc_number: 17151121997, policy_type: 1, compensation: 0 },
    { flight:'EK202', sta: 1568321000, acc_number: 17151121999, policy_type: 1, compensation: 0 },
    { flight:'EK347', sta: 1568316000, acc_number: 17151121990, policy_type: 1, compensation: 0 }
];
connecting = [
    { flight:'EK029', sta: 1568343600, acc_number: 17151121993, policy_type: 1, compensation: 10 },
    { flight:'EK029', sta: 1568343600, acc_number: 17151121994, policy_type: 1, compensation: 10 },
    { flight:'EK002', sta: 1568340000, acc_number: 17151121995, policy_type: 1, compensation: 10 },
    { flight:'EK029', sta: 1568343600, acc_number: 17151121996, policy_type: 1, compensation: 10 },
    { flight:'EK025', sta: 1568343600, acc_number: 17151121997, policy_type: 1, compensation: 10 },
    { flight:'EK002', sta: 1568340000, acc_number: 17151121999, policy_type: 1, compensation: 10 },
    { flight:'EK201', sta: 1568343600, acc_number: 17151121990, policy_type: 1, compensation: 10 }
];

//1568287200
function timePrinter(ms){
  var time = new Date(ms)
  //var ms = time.getMilliseconds()
  //time = Math.floor(time / 1000)
  //var s = time.getSeconds()
  //time = Math.floor(time / 60)
  //var m = time.getMinutes()
  //time = Math.floor(time / 60)
  //var h = time.getHours()//GMT+8
  //return '['+('00' + h).substr(-2)+':'+('00' + m).substr(-2)+':'+('00' + s).substr(-2)+':'+('000' + ms).substr(-3)+'] '
  return time.toUTCString();
}

const readlineLib = require('readline');
const readline = readlineLib.createInterface({
  input: process.stdin,
  output: process.stdout
});

async function writeCSV() {
  console.log('Start writing CSV');
  const createCsvWriter = require('csv-writer').createObjectCsvWriter;
  const csvWriter = createCsvWriter({
    path: 'out.csv',
    header: [
      {id: 'policy_no', title: 'PolicyNo'},
      {id: 'flight', title: 'Flight'},
      {id: 'sta', title: 'Schedule Arrival Time'},
      {id: 'acc_number', title: 'Account Number'},
      {id: 'policy_type ', title: 'Policy type'},
      {id: 'atd', title: 'Actual Departure Time'},
      {id: 'ata', title: 'Actual Arrival Time'},
      {id: 'connect_from', title: 'policy connect from'},
      {id: 'coonect_to', title: 'policy connect to'},
      {id: 'status', title: 'STATUS'},
      {id: 'compensation', title: 'Compensation'}
    ]
  });
  const result = await rpc.get_table_rows({
    json: true,              
    code: 'assurancehlg',
    scope: 'assurancehlg',
    table: 'pinsurance2',
    reverse : false,
    limit: 100
    // index_position: 3,
    // key_type: 'i64',
    // lower_bound : this.state.textFieldValue,
    // upper_bound : this.state.textFieldValue
  });
  const data = [];
  for(var i = 0; i < result.rows; i++) {
    data.push({
      policy_no: result.rows[i].policy_no,
      flight: result.rows[i].flight,
      sta: result.rows[i].sta,
      acc_number: result.rows[i].acc_number,
      policy_type: result.rows[i].policy_type,
      atd: result.rows[i].atd,
      ata: result.rows[i].ata,
      connect_to: result.rows[i].connect_to,
      connect_from: result.rows[i].connect_from,
      status: result.rows[i].status,
      compensation: result.rows[i].compensation
    });
  }
  await csvWriter.writeRecords(data);
  console.log('Done writing CSV');
}

async function read() {
  readline.question(`Enter to next event`, async (name) => {
    eventLists.reverse();
    var e = eventLists.pop();
    eventLists.reverse();
    console.log('\x1b[32m%s\x1b[0m', `Flight ${e.flight.flight_number} ${e.event} ${timePrinter(e.time*1000)}`);
    if(e.event == 'departed') {
      flightdepart(e.flight.flight_number, e.flight.flight_timestamp, e.time);
    } else if(e.event == 'arrived'){
      flightarrive(e.flight.flight_number, e.flight.flight_timestamp, e.time);
    } else { // cancellation
      flightcancel(e.flight.flight_number, e.flight.flight_timestamp);
    }
    if(eventLists.length > 0) {
      read();
    } else {
      readline.close();
      writeCSV();
    }
  });
}

async function presetInsurance() {
  console.log('Preset Inusrance');
  var results = [];
  for(var i = 0; i < 3; i++) {
      console.log(i);
      results.push(api.transact({
          actions: [{
            account: 'assurancehlg',
            name: 'add1',
            authorization: [{
              actor: 'assurancehlg',
              permission: 'active',
            }],
            data: {
              flight: insuranceList[i].flight, 
              sta: insuranceList[i].sta, 
              acc_number: insuranceList[i].acc_number, 
              policy_type: insuranceList[i].policy_type, 
              compensation: insuranceList[i].compensation,
              connect_to: 0, 
              connect_from: 0
            },
          }]
      }, {
          blocksBehind: 3,
          expireSeconds: 30,
      }).catch(function(e){
          console.log(e);
      }));
  }
  for(var i = 3; i < 10; i++) {
    console.log(i);
    results.push(api.transact({
        actions: [{
          account: 'assurancehlg',
          name: 'addconnect',
          authorization: [{
            actor: 'assurancehlg',
            permission: 'active',
          }],
          data: {
            flight1: insuranceList[i].flight, 
            sta1: insuranceList[i].sta, 
            flight2: connecting[i-3].flight, 
            sta2: connecting[i-3].sta, 
            acc_number: insuranceList[i].acc_number,  
            compensation: insuranceList[i].compensation,
          },
        }]
    }, {
        blocksBehind: 3,
        expireSeconds: 30,
    }).catch(function(e){
        console.log(e);
    }));
  }
  for(r in results){
      await results[r];
  }

  console.log('\x1b[32m%s\x1b[0m', `Complete preset`);
  read();
}

presetInsurance();