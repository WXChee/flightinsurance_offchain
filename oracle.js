global.fetch = require("node-fetch");
global.WebSocket = require("ws");

request = require('request')
const fetch = require('node-fetch');
const { TextEncoder, TextDecoder } = require('util');
const { Api, JsonRpc, RpcError } = require('eosjs');
const { JsSignatureProvider } = require('eosjs/dist/eosjs-jssig'); 

const { createDfuseClient, InboundMessageType } = require("@dfuse/client")

const client = createDfuseClient({ apiKey: "server_36ca10151a5d744ee6a28e685ba68b51", network: "jungle" })

const oraclePrivateKey = "5Km2kuu7vtFDPpxywn4u3NKydrLLG5isZ6rYkcw3xsH9hNYeA6i";
const hlgPrivateKey = "5JfbS6SH5bk8F4GUVANGX68e3GQ8BbRtDQZNpNebJo8EkSbfK32";
const signatureProvider = new JsSignatureProvider([oraclePrivateKey, hlgPrivateKey]);

const rpc = new JsonRpc('https://jungle2.cryptolions.io:443', { fetch });

const api = new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() });

async function flightdepart(flight, sta, atd){
  const result = await api.transact({
    actions: [{
      account: 'assurancehlg',
      name: 'fdepart',
      authorization: [{
        actor: 'flightoracle',
        permission: 'active',
      }],
      data: {
        flight: flight,
        sta: sta,
        atd: atd
      },
    }]
  }, {
    blocksBehind: 3,
    expireSeconds: 30,
  }).catch(async function(e){
    console.log('\x1b[33m%s\x1b[0m', '\tGot error, resend');
    await flightdepart(flight, sta, atd);
  });
  console.dir(`Flight ${flight} is departed on ${timePrinter(atd*1000)}, set on smart contract`);
}

async function flightarrive(flight, sta, ata){
  const result = await api.transact({
    actions: [{
      account: 'assurancehlg',
      name: 'farrive',
      authorization: [{
        actor: 'flightoracle',
        permission: 'active',
      }],
      data: {
        flight: flight,
        sta: sta,
        ata: ata
      },
    }]
  }, {
    blocksBehind: 3,
    expireSeconds: 30,
  }).catch(async function(e){
    console.log('\x1b[33m%s\x1b[0m', '\tGot error, resend');
    await flightarrive(flight, sta, ata);
  });
  console.dir(`Flight ${flight} is arrived on ${timePrinter(ata*1000)}, set on smart contract`);
}

async function flightcancel(flight, sta, cancelt){
  const result = await api.transact({
    actions: [{
      account: 'assurancehlg',
      name: 'flightcl2',
      authorization: [{
        actor: 'flightoracle',
        permission: 'active',
      }],
      data: {
        flight: flight,
        sta: sta,
        cancelt: cancelt
      },
    }]
  }, {
    blocksBehind: 3,
    expireSeconds: 30,
  }).catch(async function(e){
    console.log('\x1b[33m%s\x1b[0m', '\tGot error, resend');
    await flightcancel(flight, sta, cancelt);
  });
  console.dir(`Flight ${flight} is cancelled, set on smart contract`);
}

function timePrinter(ms){
  var time = new Date(ms);
  return time.toUTCString();
}

console.log();
console.log('This program simulate the workflow of Oracle Server');
console.log();
console.log('basically the orcale server is a server that function as a medium between flight API and blockchain');
console.log('for now, the API is a simulated API from the test program');
console.log('\n');

// Server Oracle
var express = require("express");
var app = express();
var bodyParser = require('body-parser')
app.use(bodyParser.json());
// app.get("/event", (req, res, next) => {
//   console.log(req.params);
//   res.json({
//     success: true
//   })
// });
app.post("/event", async (req, res, next) => {
  console.log('Received from API push to EOSIO');
  if(req.body.event == 'departed') {
    await flightdepart(req.body.flight.flight_number, req.body.flight.flight_timestamp, req.body.time);
  } else if(req.body.event == 'arrived'){
    await flightarrive(req.body.flight.flight_number, req.body.flight.flight_timestamp, req.body.time);
  } else { // cancellation
    await flightcancel(req.body.flight.flight_number, req.body.flight.flight_timestamp, req.body.time);
  }
  res.json({
    success: true
  })
});
app.listen(4001, () => {
 console.log("Oracle Server running on port 4001");
});