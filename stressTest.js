global.fetch = require("node-fetch");
global.WebSocket = require("ws");

request = require('request')
const fetch = require('node-fetch');
const { TextEncoder, TextDecoder } = require('util');
const { Api, JsonRpc, RpcError } = require('eosjs');
const { JsSignatureProvider } = require('eosjs/dist/eosjs-jssig'); 

// const { createDfuseClient, InboundMessageType } = require("@dfuse/client")

// const client = createDfuseClient({ apiKey: "server_36ca10151a5d744ee6a28e685ba68b51", network: "jungle" })

const oraclePrivateKey = "5Km2kuu7vtFDPpxywn4u3NKydrLLG5isZ6rYkcw3xsH9hNYeA6i";
const hlgPrivateKey = "5JfbS6SH5bk8F4GUVANGX68e3GQ8BbRtDQZNpNebJo8EkSbfK32";
const signatureProvider = new JsSignatureProvider([oraclePrivateKey, hlgPrivateKey]);

const rpcList = [];
rpcList.push(new JsonRpc('https://jungle2.cryptolions.io:443', { fetch }));
//rpcList.push(new JsonRpc('https://api.jungle.alohaeos.com:443', { fetch }));
//rpcList.push(new JsonRpc('http://jungle.eosamsterdam.net:80', { fetch }));
//rpcList.push(new JsonRpc('http://jungle.eosbcn.com:8080', { fetch }));
//rpcList.push(new JsonRpc('http://145.239.133.201:8888', { fetch }));

const apiList = [];
for(var i in rpcList) {
    var rpc = rpcList[i];
    apiList.push(new Api({ rpc, signatureProvider, textDecoder: new TextDecoder(), textEncoder: new TextEncoder() }));
}

var completed = 0;
var failed = 0;

async function stressTest() {
    console.log('START');
    var results = [];
    for(var i = 0; i < 250; i++) {
        console.log(i);
        for(var j in apiList) {
            results.push(apiList[j].transact({
                actions: [{
                  account: 'eosio.token',
                  name: 'transfer',
                  authorization: [{
                    actor: 'flightoracle',
                    permission: 'active',
                  }],
                  data: {
                    from:'flightoracle',
                    to:'assurancehlg',
                    quantity:'0.0001 JUNGLE',
                    memo:'stress test ' + i.toString()
                  },
                }]
            }, {
                blocksBehind: 3,
                expireSeconds: 60,
            }).then(function(value){
                console.log('\x1b[32m%s\x1b[0m', `completed`);
                completed++;
            }).catch(function(e){
                console.log('\x1b[31m%s\x1b[0m', `failed ${e}`);
                failed++;
            }));
        }
        await sleep(20);
    }
    for(r in results){
        await results[r];
    }

    console.log('COMPLETED STRESS TEST');
    console.log('\x1b[32m%s\x1b[0m', `${completed} completed`);
    console.log('\x1b[31m%s\x1b[0m', `${failed} failed`);
}

function sleep(ms){
    return new Promise(resolve=>{
        setTimeout(resolve,ms)
    })
}

console.log('The stress test will start in 2 seconds');

setTimeout(stressTest, 2000);
