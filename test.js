var fs = require('fs');

const TIME_BETWEEN_THRESHOLD = 3600;

global.fetch = require("node-fetch");
global.WebSocket = require("ws");

request = require('request');

var eventList = [];
var flightDir = {};

async function readEventList() {
    data = fs.readFileSync('source/flights.csv', 'utf8');
    data = data.substring(data.search('\n')+1, data.length);
    while(true){
        var line = data.substring(0, data.search('\n'));
        if (line.length == 0) break;
        data = data.substring(data.search('\n')+1, data.length);
        var flight = line.substring(0, line.search(','));
        line = line.substring(line.search(',')+1, line.length);
        var staString = line.substring(0, line.search(','));
        line = line.substring(line.search(',')+1, line.length);
        var atdString = line.substring(0, line.search(','));
        line = line.substring(line.search(',')+1, line.length);
        var ataString = line.substring(0, line.search(','));
        line = line.substring(line.search(',')+1, line.length);
        var cancelString = line.substring(0, line.search(','));
        line = line.substring(line.search(',')+1, line.length);
        var price = parseInt(line);
        flightDir[`${flight}:${staString}`] = {
            atd: atdString == '0'? 0:(new Date(atdString)).getTime() / 1000,
            ata: ataString == '0'? 0:(new Date(ataString)).getTime() / 1000,
            cancel: cancelString == '0'? 0:(new Date(cancelString)).getTime() / 1000,
            price: price
        };
        // console.log(line);
        // console.log(`| ${flight} | ${staString} | ${atdString} | ${ataString} | ${cancelString} | ${price} `)
        if(atdString != '0') {
            var event = {
                flight:{
                  flight_number: flight,
                  flight_timestamp: (new Date(staString)).getTime() / 1000
                },
                event:'departed',
                time:(new Date(atdString)).getTime() / 1000
            };
            eventList.push(event);
        }
        if(ataString != '0') {
            var event = {
                flight:{
                  flight_number: flight,
                  flight_timestamp: (new Date(staString)).getTime() / 1000
                },
                event:'arrived',
                time:(new Date(ataString)).getTime() / 1000
            };
            eventList.push(event);
        }
        if(cancelString != '0') {
            var event = {
                flight:{
                  flight_number: flight,
                  flight_timestamp: (new Date(staString)).getTime() / 1000
                },
                event:'cancel',
                time:(new Date(cancelString)).getTime() / 1000
            };
            eventList.push(event);
        }
    }
    //console.log(eventList);
    eventList.sort(function(a,b){
        return a.time - b.time;
    });
    return;
}

testCaseInsurances = [];
function pFromString(p) {
    var policy = {};
    policy.flight = p.substring(0, p.search(','));
    p = p.substring(p.search(',')+1, p.length);
    policy.flightString = policy.flight+':'+p.substring(0, p.search(','));
    policy.sta = (new Date(p.substring(0, p.search(',')))).getTime() / 1000;
    p = p.substring(p.search(',')+1, p.length);
    policy.acc_number = parseInt(p.substring(0, p.search(',')));
    p = p.substring(p.search(',')+1, p.length);
    policy.policy_type = p.substring(0, p.search(',')) == 'premium'? 1:0;
    p = p.substring(p.search(',')+1, p.length);
    policy.compensation = parseFloat(p);
    
    return policy;
}
async function readTestCaseInsurances() {
    data = fs.readFileSync('source/case/testCaseList.csv', 'utf8');
    while(true) {
        var line = data.substring(0, data.search('\n'));
        var insurance = [];
        while(true){
            var p = line.substring(0, line.search(',,'));
            if(p.length <= 0) break;
            insurance.push(pFromString(p));
            line = line.substring(line.search(',,')+2, line.length);
            if(line.length <= 0) break;
        }
        var price = 0;
        for(p in insurance) {
            price += flightDir[insurance[p].flightString].price;
        }
        insurance[insurance.length-1].compensation = price;
        testCaseInsurances.push(insurance);
        data = data.substring(data.search('\n')+1, data.length);
        if(data.length <= 0) break;
    }
}



// TEST SERVER
function pushEvent(event, resend) {
    return new Promise(function (resolve, reject) {
        request({
            url: 'http://localhost:4001/event',
            method: 'POST',
            body: event,
            json: true
        }, function(err, response, body){
            if (!err && response.statusCode == 200) {
                resolve(body);
            } else {
                if(resend < 2) {
                    console.log('\x1b[31m%s\x1b[0m', `\tConnection issue, resend`);
                    return pushEvent(event, resend+1);
                }
                console.log('\x1b[31m%s\x1b[0m', `Connection issue, reject`);
                reject(error);
            }
        });
    });
}

function pushPolicy(policy, resend, connecting) {
    return new Promise(function (resolve, reject) {
        request({
            url: `http://localhost:4002/${connecting? 'add_connect':'add_single'}`,
            method: 'POST',
            body: policy,
            json: true
        }, function(err, response, body){
            if (!err && response.statusCode == 200) {
                resolve(body);
            } else {
                if(resend < 2) {
                    console.log('\x1b[31m%s\x1b[0m', `\tConnection issue, resend`);
                    return pushEvent(policy, resend+1, connecting);
                }
                console.log('\x1b[31m%s\x1b[0m', `Connection issue, reject`);
                reject(error);
            }
        });
    });
}
// TEST SERVER






// Ready for CSV
const fetch = require('node-fetch');
const { Api, JsonRpc, RpcError } = require('eosjs');
const rpc = new JsonRpc('https://jungle2.cryptolions.io:443', { fetch });
var dataR = [];
var policy2index = {};
function status2String(s) {
    if(s == 255) return 'WAITING'
    if(s == 0) return 'NO DELAY'
    if(s == 1) return 'DELAY 1H'
    if(s == 2) return 'DELAY 2H'
    if(s == 3) return 'DELAY 3H'
    if(s == 31) return 'MISCONNECTION'
    if(s == 63) return 'CANCELLATION'
    if(s == 64) return 'CANCELLATION EARLIER'
    return 'unknown'
}

function status2Amount(s, price, type) {
    if(s == 'WAITING') return 0
    if(s == 'NO DELAY') return 0;
    if(s == 'DELAY 1H') return type? Math.floor(price * 0.3):Math.floor(price * 0.8);
    if(s == 'DELAY 2H') return type? Math.floor(price * 0.6):Math.floor(price * 0.8);
    if(s == 'DELAY 3H') return type? price:Math.floor(price * 0.8);
    if(s == 'MISCONNECTION') return type? price:0;
    if(s == 'CANCELLATION') return price;
    if(s == 'CANCELLATION EARLIER') return 0;
    return 0
}

function expectedStatus(policy) {
    if(policy.from_connection == 0) {
        if(flightDir[policy.flight+':'+policy.sta].cancel != 0) {
            if((new Date(policy.sta)).getTime()/1000 - flightDir[policy.flight+':'+policy.sta].cancel >= 604800) {
                return status2String(64);
            }
            return status2String(63);
        }
        var delay = Math.floor((flightDir[policy.flight+':'+policy.sta].ata - ((new Date(policy.sta)).getTime()/1000)) / 3600);
        if(delay < 0) {
            return status2String(0);
        } else if(delay < 3) {
            return status2String(delay);
        } else {
            return status2String(3);
        }
    } else {
        if(flightDir[policy.flight+':'+policy.sta].atd == 0) {
            return status2String(63);
        }
        policyFrom = dataR[policy2index[policy.from_connection]];
        if(policyFrom.e_status == 'CANCELLATION') {
            return status2String(31);
        }
        var time_between = flightDir[policy.flight+':'+policy.sta].atd - flightDir[policyFrom.flight+':'+policyFrom.sta].ata;
        if(time_between < TIME_BETWEEN_THRESHOLD) {
            return status2String(31);
        }
        var delay = Math.floor((flightDir[policy.flight+':'+policy.sta].ata - ((new Date(policy.sta)).getTime()/1000)) / 3600);
        if(delay < 3) {
            return status2String(delay);
        } else {
            return status2String(3);
        }
    }
}

async function writeExpectedCSV() {
    console.log('Start writing CSV');
    const createCsvWriter = require('csv-writer').createObjectCsvWriter;
    //var csvResult = 'PolicyNo,Flight,Schedule Arrival Time,Account Number,Policy type,Expected STATUS';
    const csvWriter = createCsvWriter({
        path: 'source/result/policiesExpectedList.csv',
        header: [
            {id: 'policy_no', title: 'PolicyNo'},
            {id: 'flight', title: 'Flight'},
            {id: 'sta', title: 'Schedule Arrival Time'},
            {id: 'acc_number', title: 'Account Number'},
            {id: 'blank_1', title: 'Policy type'},
            //{id: 'type ', title: 'Policy type'},
            {id: 'e_status', title: 'Expected STATUS'},
            {id: 'a_status', title: 'Actual STATUS'},
            {id: 'e_compensation', title: 'Expected Compensation'},
            {id: 'a_compensation', title: 'Actual Compensation'},
            {id: 'atd', title: 'Actual Departure Time'},
            {id: 'ata', title: 'Actual Arrival Time'},
            {id: 'to_connection', title: 'policy connect to'},
            {id: 'from_connection', title: 'policy connect from'}
        ]
    });
    const result = await rpc.get_table_rows({
        json: true,              
        code: 'assurancehlg',
        scope: 'assurancehlg',
        table: 'insurance2',
        reverse : false,
        limit: 1000
        // index_position: 3,
        // key_type: 'i64',
        // lower_bound : this.state.textFieldValue,
        // upper_bound : this.state.textFieldValue
    });
    var data = [];
    for(var i = 0; i < result.rows.length; i++) {
        data.push({
            policy_no: result.rows[i].policy_no,
            flight: result.rows[i].flight,
            sta: (new Date(result.rows[i].sta * 1000)).toISOString(),
            acc_number: result.rows[i].acc_number,
            //policyType: result.rows[i].policy_type==1? 'premium':'lite',
            blank_1: result.rows[i].policy_type==1? 'premium':'lite',
            e_status: 'NO DELAY',
            e_compensation: 0,
            atd: result.rows[i].atd,
            ata: result.rows[i].ata,
            from_connection: result.rows[i].connect_from,
            to_connection: result.rows[i].connect_to
        });
        policy2index[result.rows[i].policy_no] = data.length - 1;
    }
    dataR = data;
    for(d in data) {
        dataR[d].e_status = expectedStatus(dataR[d]);
        var price = flightDir[dataR[d].flight+':'+dataR[d].sta].price;
        if(dataR[d].to_connection != 0) {
            price = 0;
        } else if (dataR[d].from_connection != 0) {
            if(policy2index[dataR[d].from_connection] == undefined){
                continue;
            }
            price += flightDir[dataR[policy2index[dataR[d].from_connection]].flight+':'+dataR[policy2index[dataR[d].from_connection]].sta].price;
        }
        dataR[d].e_compensation = status2Amount(dataR[d].e_status, price, dataR[d].blank_1=='premium');
    }
    data = dataR;
    await csvWriter.writeRecords(data);
}

async function writeResultCSV() {
    console.log('Start writing CSV');
    const createCsvWriter = require('csv-writer').createObjectCsvWriter;
    //var csvResult = 'PolicyNo,Flight,Schedule Arrival Time,Account Number,Policy type,Expected STATUS';
    const csvWriter = createCsvWriter({
        path: 'source/result/policiesResultList.csv',
        header: [
            {id: 'policy_no', title: 'PolicyNo'},
            {id: 'flight', title: 'Flight'},
            {id: 'sta', title: 'Schedule Arrival Time'},
            {id: 'acc_number', title: 'Account Number'},
            {id: 'blank_1', title: 'Policy type'},
            //{id: 'type ', title: 'Policy type'},
            {id: 'e_status', title: 'Expected STATUS'},
            {id: 'a_status', title: 'Actual STATUS'},
            {id: 'e_compensation', title: 'Expected Compensation'},
            {id: 'a_compensation', title: 'Actual Compensation'},
            {id: 'atd', title: 'Actual Departure Time'},
            {id: 'ata', title: 'Actual Arrival Time'},
            {id: 'to_connection', title: 'policy connect to'},
            {id: 'from_connection', title: 'policy connect from'}
        ]
    });
    const result = await rpc.get_table_rows({
        json: true,              
        code: 'assurancehlg',
        scope: 'assurancehlg',
        table: 'pinsurance2',
        reverse : false,
        limit: 1000
        // index_position: 3,
        // key_type: 'i64',
        // lower_bound : this.state.textFieldValue,
        // upper_bound : this.state.textFieldValue
    });
    for(var i = 0; i < result.rows.length; i++) {
        if(policy2index[result.rows[i].policy_no] == undefined) continue;
        dataR[policy2index[result.rows[i].policy_no]].a_status = status2String(result.rows[i].status);
        dataR[policy2index[result.rows[i].policy_no]].a_compensation = status2Amount(dataR[policy2index[result.rows[i].policy_no]].a_status, result.rows[i].compensation, result.rows[i].policy_type==1);
    }
    await csvWriter.writeRecords(dataR);
}
// Ready for CSV






// __________ MAIN __________ //
// wait input method
var policyEndStream;
function timePrinter(ms){
    var time = new Date(ms)
    return time.toUTCString();
}

async function preread() {
    readline.question(`ENTER TO START`, async (name) => {
        await writeExpectedCSV();
        if(eventList.length > 0) {
            return read();
        } else {
            await policyEndStream.close();
            readline.close();
            //writeCSV();
            return true;
        }
    });
}
async function postread() {
    readline.question(`ENTER TO CLOSE`, async (name) => {
        readline.close();
    });
}
var success = 0;
var fail = 0;
async function read() {
    readline.question(`Enter to next event`, async (name) => {
        eventList.reverse();
        var e = eventList.pop();
        eventList.reverse();
        console.log('\x1b[37m%s\x1b[0m', `Flight ${e.flight.flight_number} ${e.event} ${timePrinter(e.time*1000)}`);
        try {
            let r = await pushEvent(e, 0);
            if(r.success) {
                console.log('\x1b[32m%s\x1b[0m', `Flight event pushes\n`);
            }
        } catch(e){
            console.log('\x1b[31m%s\x1b[0m', `Flight event not pushes\n`);
        }
        
        while(eventList.length > 0){
            eventList.reverse();
            var e = eventList.pop();
            eventList.reverse();
            console.log('\x1b[37m%s\x1b[0m', `Flight ${e.flight.flight_number} ${e.event} ${timePrinter(e.time*1000)}`);
            try {
                let r = await pushEvent(e, 0);
                if(r.success) {
                    console.log('\x1b[32m%s\x1b[0m', `Flight event pushes\n`);
                }
            } catch(e){
                console.log('\x1b[31m%s\x1b[0m', `Flight event not pushes\n`);
            }
        }
        
        if(eventList.length > 0) {
            return read();
        } else {
            //readline.close();
            //writeCSV();
            await writeResultCSV();
            console.log(`\t\tDone all test case\t`);
            // await policyEndStream.close();
            return postread();
        }
    });
}
// wait input method

const readlineLib = require('readline');
const readline = readlineLib.createInterface({
    input: process.stdin,
    output: process.stdout
});
const { createDfuseClient, InboundMessageType } = require("@dfuse/client")
const client = createDfuseClient({ apiKey: "server_36ca10151a5d744ee6a28e685ba68b51", network: "jungle" })
// read flight event first
console.log('START');
readEventList().then(function(){
    console.log('event get');
    readTestCaseInsurances().then(async function(){
        console.log('testcase get, pushing policy');
        for(i in testCaseInsurances){
            console.log(testCaseInsurances[i][0].sta)
            if(testCaseInsurances[i].length == 2) {
                pushPolicy({
                    first: {
                        flight: testCaseInsurances[i][0].flight,
                        sta: testCaseInsurances[i][0].sta
                    },
                    second: {
                        flight: testCaseInsurances[i][1].flight,
                        sta: testCaseInsurances[i][1].sta
                    },
                    acc_number: testCaseInsurances[i][0].acc_number,
                    compensation: testCaseInsurances[i][1].compensation
                }, 0, true);
            }else{
                pushPolicy({
                    flight: testCaseInsurances[i][0].flight,
                    sta: testCaseInsurances[i][0].sta,
                    acc_number: testCaseInsurances[i][0].acc_number,
                    policy_type: testCaseInsurances[i][0].policy_type,
                    compensation: testCaseInsurances[i][0].compensation
                }, 0, false);
            }
        }
        // DETECT COMPENSATION REFUND
        policyEndStream = await client.streamActionTraces({ accounts: "assurancehlg", action_names: "policyend" }, (message) => {
            if (message.type === InboundMessageType.ACTION_TRACE) {
                const { policy_no, policy_type, flight, sta, acc_number, statuss, compensation } = message.data.trace.act.data;
                console.log('\x1b[34m%s\x1b[0m', `Policy ${policy_no} ${statuss}, RM${compensation} to ${acc_number} for flight ${flight} is proccessing...`);
                if(policy2index[policy_no] == undefined) {
                    console.log('\x1b[33m%s\x1b[0m', `\t${policy_no} not found in test case`);
                } else {
                    console.log(`\tExpected : ${dataR[policy2index[policy_no]].e_status} pay compensation RM${dataR[policy2index[policy_no]].e_compensation}`);
                    if(dataR[policy2index[policy_no]].e_compensation == compensation) {
                        console.log('\x1b[32m%s\x1b[0m', `\tPolicy end correctly`);
                        success++;
                    } else {
                        console.log('\x1b[31m%s\x1b[0m', `\tPolicy end wrongly`);
                        fail++;
                    }
                }
            }
        }).catch((error) => {
            console.log("An error occurred on {assurancehlg::refund2}.", error)
        });
        await preread();
    })
})
