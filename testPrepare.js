var fs = require('fs'); 

// var time = new Date(1568351400000);
// var t = new Date(1568351500000);
// console.log(time > t);
// var t = new Date('2019-09-13T03:00:00.000Z');
// console.log(t.getTime() / 1000);


var events = [];

insuranceList = [
    { flight:'AK860', sta: 1568278800, acc_number: 17151121991, policy_type: 0, compensation: 10 },//0
    { flight:'AK861', sta: 1568284200, acc_number: 17151121992, policy_type: 0, compensation: 10 },
    { flight:'AK861', sta: 1568284200, acc_number: 17151121996, policy_type: 1, compensation: 10 },
    { flight:'AK862', sta: 1568284000, acc_number: 17151121998, policy_type: 0, compensation: 10 },
    { flight:'EK347', sta: 1568316000, acc_number: 17151121993, policy_type: 1, compensation: 0 },//4
    { flight:'EK340', sta: 1568316000, acc_number: 17151121994, policy_type: 1, compensation: 0 },
    { flight:'EK340', sta: 1568316000, acc_number: 17151121995, policy_type: 1, compensation: 0 },
    { flight:'EK316', sta: 1568319600, acc_number: 17151121996, policy_type: 1, compensation: 0 },
    { flight:'EK316', sta: 1568319600, acc_number: 17151121997, policy_type: 1, compensation: 0 },
    { flight:'EK202', sta: 1568321000, acc_number: 17151121999, policy_type: 1, compensation: 0 },
    { flight:'EK347', sta: 1568316000, acc_number: 17151121990, policy_type: 1, compensation: 0 }//10
];
connecting = [
    { flight:'EK029', sta: 1568343600, acc_number: 17151121993, policy_type: 1, compensation: 10 },
    { flight:'EK029', sta: 1568343600, acc_number: 17151121994, policy_type: 1, compensation: 10 },
    { flight:'EK002', sta: 1568340000, acc_number: 17151121995, policy_type: 1, compensation: 10 },
    { flight:'EK029', sta: 1568343600, acc_number: 17151121996, policy_type: 1, compensation: 10 },
    { flight:'EK025', sta: 1568343600, acc_number: 17151121997, policy_type: 1, compensation: 10 },
    { flight:'EK002', sta: 1568340000, acc_number: 17151121999, policy_type: 1, compensation: 10 },
    { flight:'EK201', sta: 1568343600, acc_number: 17151121990, policy_type: 1, compensation: 10 }
];

testCaseList = [
    [{ flight:'AK860', sta: 1568278800, acc_number: 17151121991, policy_type: 0, compensation: 100 }],
    [{ flight:'AK861', sta: 1568284200, acc_number: 17151121992, policy_type: 0, compensation: 100 }],
    [{ flight:'AK861', sta: 1568284200, acc_number: 17151121996, policy_type: 1, compensation: 100 }],
    [{ flight:'AK862', sta: 1568284000, acc_number: 17151121998, policy_type: 0, compensation: 100 }],
    [{ flight:'EK347', sta: 1568316000, acc_number: 17151121993, policy_type: 1, compensation: 0 }, { flight:'EK029', sta: 1568343600, acc_number: 17151121993, policy_type: 1, compensation: 100 }],
    [{ flight:'EK340', sta: 1568316000, acc_number: 17151121994, policy_type: 1, compensation: 0 }, { flight:'EK029', sta: 1568343600, acc_number: 17151121994, policy_type: 1, compensation: 100 }],
    [{ flight:'EK340', sta: 1568316000, acc_number: 17151121995, policy_type: 1, compensation: 0 }, { flight:'EK002', sta: 1568340000, acc_number: 17151121995, policy_type: 1, compensation: 100 }],
    [{ flight:'EK316', sta: 1568319600, acc_number: 17151121996, policy_type: 1, compensation: 0 }, { flight:'EK029', sta: 1568343600, acc_number: 17151121996, policy_type: 1, compensation: 100 }],
    [{ flight:'EK316', sta: 1568319600, acc_number: 17151121997, policy_type: 1, compensation: 0 }, { flight:'EK025', sta: 1568343600, acc_number: 17151121997, policy_type: 1, compensation: 100 }],
    [{ flight:'EK202', sta: 1568321000, acc_number: 17151121999, policy_type: 1, compensation: 0 }, { flight:'EK002', sta: 1568340000, acc_number: 17151121999, policy_type: 1, compensation: 100 }],
    [{ flight:'EK347', sta: 1568316000, acc_number: 17151121990, policy_type: 1, compensation: 0 }, { flight:'EK201', sta: 1568343600, acc_number: 17151121990, policy_type: 1, compensation: 100 }]
];

for (i in insuranceList) {
    insuranceList[i].sta = new Date(insuranceList[i].sta*1000).toISOString();
}

for (i in connecting) {
    connecting[i].sta = new Date(connecting[i].sta*1000).toISOString();
}

console.log(insuranceList);
console.log(connecting);

var accDir = {};

for (i in insuranceList) {
    if(! (insuranceList[i].acc_number.toString() in accDir)) {
        accDir[insuranceList[i].acc_number] = {
            balance: 100,
            policies: [],
            transactions: [{
                memo: 'balance b/f',
                amount: 100
            }]
        };
    }
    if(i < 4) {
        accDir[insuranceList[i].acc_number].balance -= insuranceList[i].compensation;
        accDir[insuranceList[i].acc_number].policies.push({
            flight: insuranceList[i].flight,
            time: insuranceList[i].sta,
            policy_type: insuranceList[i].policy_type == 1? 'premium':'lite',
            price: insuranceList[i].compensation
        });
        accDir[insuranceList[i].acc_number].transactions.push({
            memo: 'buy policy',
            amount: -insuranceList[i].compensation
        })
    } else {
        accDir[connecting[i-4].acc_number].balance -= connecting[i-4].compensation;
        accDir[connecting[i-4].acc_number].policies.push({
            flight: connecting[i-4].flight,
            time: connecting[i-4].sta,
            policy_type: connecting[i-4].policy_type == 1? 'premium':'lite',
            price: connecting[i-4].compensation
        });
        accDir[connecting[i-4].acc_number].policies.push({
            flight: insuranceList[i].flight,
            time: insuranceList[i].sta,
            policy_type: insuranceList[i].policy_type == 1? 'premium':'lite',
            price: insuranceList[i].compensation
        });
        accDir[connecting[i-4].acc_number].transactions.push({
            memo: 'buy policy',
            amount: -connecting[i-4].compensation
        })
    }
}

for (var acc in accDir) {
    accDetailString = '';
    accDetailString += `Acc Number,${acc}\n`;
    accDetailString += `\nPolicies hold\n`;
    accDetailString += `Flight,Time,Type,Flight Price\n`;
    for(i in accDir[acc].policies) {
        accDetailString += `${accDir[acc].policies[i].flight},${accDir[acc].policies[i].time},${accDir[acc].policies[i].policy_type},${accDir[acc].policies[i].price}\n`;
    }
    accDetailString += '\nTransaction\n';
    for(i in accDir[acc].transactions) {
        accDetailString += `${i},${accDir[acc].transactions[i].memo},${accDir[acc].transactions[i].amount}\n`;
    }

    fs.writeFile(`source/case/acc/acc_${acc}.csv`, accDetailString, function (err) {
        if (err) throw err;
        console.log('Saved!');
    });
}



testCaseString = '';
for(t in testCaseList){
    //testCaseString += ','
    for(p in testCaseList[t]) {
        testCaseString += `${testCaseList[t][p].flight},${(new Date(testCaseList[t][p].sta * 1000)).toISOString()},${testCaseList[t][p].acc_number},${testCaseList[t][p].policy_type == 1? 'premium':'lite'},${testCaseList[t][p].compensation},,`; 
    }
    testCaseString += '\n'
}
fs.writeFile(`source/case/testCaseList.csv`, testCaseString, function (err) {
    if (err) throw err;
    console.log('Saved testCase!');
});

// var accListString = 'Account Number, Initial balance, Expected final balance\n';

// for (a in Object.keys(accDir)) {
//     accListString += Object.keys(accDir)[a]+',100,'+accDir[Object.keys(accDir)[a]].balance+'\n';
// }

