const fetch = require('node-fetch');
const { Api, JsonRpc, RpcError } = require('eosjs');
const rpc = new JsonRpc('https://jungle2.cryptolions.io:443', { fetch });

async function writeCSV() {
    console.log('Start writing CSV');
    const createCsvWriter = require('csv-writer').createObjectCsvWriter;
    const csvWriter = createCsvWriter({
      path: 'out.csv',
      header: [
        {id: 'policy_no', title: 'PolicyNo'},
        {id: 'flight', title: 'Flight'},
        {id: 'sta', title: 'Schedule Arrival Time'},
        {id: 'acc_number', title: 'Account Number'},
        {id: 'policy_type ', title: 'Policy type'},
        {id: 'atd', title: 'Actual Departure Time'},
        {id: 'ata', title: 'Actual Arrival Time'},
        {id: 'connect_from', title: 'policy connect from'},
        {id: 'coonect_to', title: 'policy connect to'},
        {id: 'status', title: 'STATUS'},
        {id: 'compensation', title: 'Compensation'}
      ]
    });
    const result = await rpc.get_table_rows({
      json: true,              
      code: 'assurancehlg',
      scope: 'assurancehlg',
      table: 'pinsurance2',
      limit: 100,
      reverse : false,
      // index_position: 3,
      // key_type: 'i64',
      // lower_bound : this.state.textFieldValue,
      // upper_bound : this.state.textFieldValue
    });
    const data = [];
    console.log(result.rows.length)
    for(var i = 0; i < result.rows.length; i++) {
      data.push({
        policy_no: result.rows[i].policy_no,
        flight: result.rows[i].flight,
        sta: result.rows[i].sta,
        acc_number: result.rows[i].acc_number,
        policy_type: result.rows[i].policy_type,
        atd: result.rows[i].atd,
        ata: result.rows[i].ata,
        connect_to: result.rows[i].connect_to,
        connect_from: result.rows[i].connect_from,
        status: result.rows[i].status,
        compensation: result.rows[i].compensation
      });
    }
    console.log(data.length);
    await csvWriter.writeRecords(data);
    console.log('Done writing CSV');
  }

  writeCSV();
